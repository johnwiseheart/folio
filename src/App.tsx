import * as React from "react";
import { style } from "typestyle";
import { getOtherProjects, getProjects, IProject } from "./utils";
import { Project } from "./Project";

interface AppState {
  projects: IProject[];
}

class App extends React.Component<{}, AppState> {
  public state = {
    projects: undefined
  };

  componentWillMount() {
    Promise.all([getProjects(), getOtherProjects()])
      .then(projects => projects.reduce((acc, val) => acc.concat(val), []))
      .then(projects => {
        this.setState({ projects });
      });
  }

  render() {
    return (
      <div className={Styles.container}>
        <div className={Styles.heading}>John Wiseheart</div>
        <p>
          Hi there! I'm a full-stack software engineer from Sydney currently
          living in the London and working at Palantir. I studied Computer
          Science at UNSW and graduated in 2016. I prefer working with React,
          Javascript and Python, but also have skills in PHP, Java and C, as
          well as a variety of other libraries and plugins.
        </p>
        <p>
          I like to bake! I keep a list of the recipes that I've made on my{" "}
          <a href="https://recipes.jcaw.me">blog</a>, and regularly update them
          as I make improvements.
        </p>
        <h2>Personal Projects</h2>
        {this.renderPersonal() || "Loading..."}
        <h2>Other Projects</h2>
        {this.renderOther() || "Loading..."}
        <h2>Contact Me</h2>
        You can contact me by email at johnwiseheart@gmail.com.
      </div>
    );
  }

  renderPersonal = () => {
    const { projects } = this.state;
    if (projects === undefined) {
      return undefined;
    }

    if (projects.length === 0) {
      return "No projects found";
    }

    return projects
      .filter(project => project.maintained)
      .sort((a, b) => (a.name > b.name ? 1 : -1))
      .map(Project);
  };

  renderOther = () => {
    const { projects } = this.state;
    if (projects === undefined) {
      return undefined;
    }

    if (projects.length === 0) {
      return "No projects found";
    }

    return projects
      .filter(project => !project.maintained)
      .sort((a, b) => (a.name > b.name ? 1 : -1))
      .map(Project);
  };
}

export default App;

namespace Styles {
  export const container = style({
    width: 700,
    paddingLeft: 50,
    paddingTop: 30
  });

  export const heading = style({
    fontSize: 25,
    marginBottom: 20,
    fontWeight: 600
  });

  export const headingSmall = style({
    fontSize: 20,
    marginTop: 15,
    marginBottom: 15,
    fontWeight: 600
  });
}
