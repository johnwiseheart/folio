import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { cssRaw, cssRule } from "typestyle";
import { normalize, setupPage } from "csstips";

normalize();
setupPage("#root");

cssRule("body", {
  fontFamily: "monospace"
});

ReactDOM.render(<App />, document.getElementById("root"));
