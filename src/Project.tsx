import * as React from "react";
import { IProject } from "./utils";
import { style } from "typestyle";
import { horizontal, padding } from "csstips";

export const Project = (project: IProject) => {
  return (
    <div className={Styles.container} key={project.name}>
      <div className={Styles.label}>
        <a href={project.url}>{project.name}</a>
      </div>
      <div className={Styles.separator}>:</div>
      {project.description}
    </div>
  );
};

namespace Styles {
  export const container = style(horizontal, padding(1, 0));

  export const label = style(
    {
      minWidth: 130
    },
    padding(0, 2)
  );

  export const separator = style(padding(0, 2));
}
