export interface IProject {
  name: string;
  url: string;
  repo_url: string;
  description: string;
  created_at: string;
  updated_at: string;
  maintained: boolean;
}

export const getProjects = async (): Promise<IProject[]> => {
  const projects = await fetch(
    "https://gitlab.com/api/v4/users/johnwiseheart/projects"
  ).then(resp => resp.json());

  return projects
    .filter(
      project =>
        project.tag_list.indexOf("website") > -1 ||
        project.tag_list.indexOf("website-demo") > -1
    )
    .map(project => {
      const {
        created_at,
        last_activity_at,
        name,
        description,
        tag_list,
        web_url
      } = project;

      const urlTag = tag_list.find(tag => tag.startsWith("url="));
      const url = urlTag !== undefined ? urlTag.split("url=")[1] : undefined;

      return {
        name,
        description,
        url,
        repo_url: web_url,
        created_at,
        updated_at: last_activity_at,
        maintained: tag_list.indexOf("website-demo") > -1
      };
    });
};

export const getOtherProjects = (): Promise<IProject[]> => {
  return new Promise(resolve => resolve(require("../other-projects.json")));
};
